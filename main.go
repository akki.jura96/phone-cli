/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>

*/
package main

import "gitlab.com/akki.jura96/phone-cli/cmd"

func main() {
	cmd.Execute()
}
